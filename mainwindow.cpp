#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "leitura.h"
#include "bancodados.h"

#include <QtDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    Lista lista;
    Leitura leitura;
    BancoDados db;

    leitura.temperatura = 25.5;
    leitura.estadoResistor = 1;
    leitura.estadoVentoinha = 0;
    leitura.limiteMaximo = 30;
    leitura.limiteMinimo = 25;
    leitura.data = "11/15 12:10";
    lista.adicionar(leitura);

    leitura.temperatura = 26.5;
    leitura.estadoResistor = 1;
    leitura.estadoVentoinha = 0;
    leitura.limiteMaximo = 30;
    leitura.limiteMinimo = 25;
    leitura.data = "11/15 12:11";
    lista.adicionar(leitura);

    leitura.temperatura = 27.5;
    leitura.estadoResistor = 0;
    leitura.estadoVentoinha = 1;
    leitura.limiteMaximo = 30 ;
    leitura.limiteMinimo = 25;
    leitura.data = "11/15 12:12";
    lista.adicionar(leitura);

    lista.imprimir();

    db.salvar();

    lista.deletarUltimo();

    qDebug() << "Deleta o ultimo" << endl;

    lista.imprimir();

}

MainWindow::~MainWindow()
{
    delete ui;
}
