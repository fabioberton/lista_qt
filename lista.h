#ifndef LISTA_H
#define LISTA_H

#include "elemento.h"

class Lista
{
    Elemento *criaElemento(Leitura l)
    {
        tam++;
        return new Elemento(l);
    }
protected:
    int tam;
    Elemento *cabeca;
public:
    Lista ()
    {
        cabeca = nullptr;
        tam = 0;
    }
    void adicionar(Leitura l);
    bool localizar(float val);
    bool deletarUltimo();
    bool limpar();
    void imprimir();
    void salvar();
};

#endif // LISTA_H
