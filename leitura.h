#ifndef LEITURA_H
#define LEITURA_H

#include <string>

using namespace std;

class Leitura
{
public:
    float temperatura;
    bool estadoVentoinha;
    bool estadoResistor;
    int limiteMaximo;
    int limiteMinimo;
    string data;
    Leitura *prox;

    Leitura()
    {
        float temperatura = 0;
        bool estadoVentoinha = false;
        bool estadoResistor = false;
        int limiteMaximo = 0;
        int limiteMinimo = 0;
        string data = "11/15";
        prox = nullptr;
    }
};

#endif // LEITURA_H
