#ifndef ELEMENTO_H
#define ELEMENTO_H

#include "leitura.h"

class Elemento
{
public:
    Leitura valor;
    Elemento *prox;
    Elemento()
    {
        prox = nullptr;
    }
    Elemento (Leitura l)
    {
        valor = l;
        prox = nullptr;
    }
};

#endif // ELEMENTO_H
