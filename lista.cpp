#include <iostream>

#include "lista.h"

using namespace std;

// Adiciona um novo elemento na lista
void Lista::adicionar(Leitura l)
{
    Elemento *novo = criaElemento(l);
    if (cabeca == nullptr)
        cabeca = novo;
    else
    {
        Elemento *onde = cabeca;
        while (onde->prox)
            onde = onde->prox;
        onde->prox = novo;
    }
}

// Busca um elemento na lista
bool Lista::localizar(float val)
{
    Elemento *ponteiro = new Elemento;
    if (!cabeca)
        return false;
    ponteiro = cabeca;
    for (; ponteiro; ponteiro = ponteiro->prox)
        if (ponteiro->valor.temperatura == val)
            return true;
    return false;
}

// Deleta o ultimo elemento
bool Lista::deletarUltimo()
{
    if (tam>0)
    {
        tam--;
        Elemento *onde = cabeca;
        while (onde->prox->prox)
            onde = onde->prox;
        free(onde->prox);
        onde->prox = nullptr;
        return true;
    }
    else
        return false;
}

bool Lista::limpar()
{
    if (tam>0){
        while(cabeca->prox==nullptr){
            deletarUltimo();
        }
        return true;
    }
    return false;
}

// Imprime a lista
void Lista::imprimir()
{
    Elemento* temp = cabeca;
    while (temp)
    {
        cout << "Temperatura: " << temp->valor.temperatura;
        cout << " Estado Ventoinha: " << temp->valor.estadoVentoinha;
        cout << " Estado Resistor: " << temp->valor.estadoResistor;
        cout << " Limite Maximo: " << temp->valor.limiteMaximo;
        cout << " Limite Minimo: " << temp->valor.limiteMinimo;
        cout << " Data: " << temp->valor.data << endl;
        temp = temp->prox;
    }
}
