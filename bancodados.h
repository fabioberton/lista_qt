#ifndef BANCODADOS_H
#define BANCODADOS_H

#include "leitura.h"
#include "lista.h"

class BancoDados
{
private:
    Lista lista;

public:
    BancoDados();
    void salvar();
    void adicionar(const Leitura &leitura);

};

#endif // BANCODADOS_H
